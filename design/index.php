<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>graphic_design</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="css/home.css" type="text/css" charset="utf-8" />


<script src="js/modernizr.custom.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script>
	var page="Agency";
</script>


</head>
<style>
	.content{
		width:78%;
		margin-left:21%;
		background-color:#F6F6F6;
		position:absolute;
		top:100%;
		
			
		}
		
		.con1{
			padding-top:5%;
			width:70%;
			margin-left:15%;
			font-family:Arial, Helvetica, sans-serif;
			font-size:25px;
			color:#FFFFFF;	
			padding-bottom:5%;
			background-color:#3A3A3A;
			text-align:center;
			}
		.con2{
			width:70%;
			margin-left:15%;
			font-family:Arial, Helvetica, sans-serif;
			font-size:25px;
			color:#666666;
			padding-bottom:2%;
			padding-top:5%;
			text-align:justify;	
			}
		.con3{
			width:70%;
			margin-left:15%;
			font-family:Arial, Helvetica, sans-serif;
			font-size:14px;
			color:#666666;
			padding-bottom:10%;
			text-align:justify;	
			
			}
		
		

</style>

<body>

<!---------------------------------------Slide images----------------------------------------------->

<div class="main">
  <ul id="cbp-bislideshow" class="cbp-bislideshow">
    <li><img src="images/background/001.jpg" /></li>
     <li><img src="images/background/002.jpg" /></li>
   
    
    
  </ul>
  <div id="cbp-bicontrols" class="cbp-bicontrols"> <span class="cbp-biprev"></span> <span class="cbp-bipause"></span> <span class="cbp-binext"></span> </div>
</div>
<script src="js/jquery.imagesloaded.min.js"></script> 
<script src="js/cbpBGSlideshow.min.js"></script> 
<script>

			$(function() {

				cbpBGSlideshow.init();

			});

		</script> 

<!------------------------------------Slide images------------------------------------------------------>



<div class="content"> 
	
	<div class="con1">
    	WORKSHOP / POSITIONING / CREATIVE STRATEGY / NAMING / ENGAGEMENT STRATEGY / CONSULTING 
    </div>
   <div class="con2">
   		Agency
   </div>
   <div class="con3">
   		We’re an independent branding agency in the kingdom of Camboida. We’re focused on Luxury and we specialize in Fashion, Hospitality,				 		Jewelry and Spirits.
        <br/> <br/> <br/>
        Our agency process combined with our expertise delivers results.
        Our Team knows what it takes to achieve your goals. 
        <br/> <br/> <br/>
        
         Since 2009, we’ve developed impactful creative solutions for our clients. Regardless of scope, we deploy a top-notch team to solve for a brand's challenge, ensuring our work really makes a difference. It's about reaching the audience, delivering a compelling message and exceeding goals.
         <br/> <br/> <br/>

Luxury brands need to focus on crafting and executing well-defined and differentiated positioning that connects with consumers. Successful brands come from powerful insights, great ideas and exceptional, consistent execution squarely aimed at results. We try to do something amazing for our clients on every single initiative. 
   </div>
   <div>
	<?php
		include('layout/footer.php');
	 ?>
</div>
   </div>
   
   
   
		<div>
	<?php 
		include('layout/header.php');
	
	?>

</div>
	<div>
	<?php 
		include('layout/menutop.php');
	
	?>

</div>


</body>
</html>
