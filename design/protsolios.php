<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>graphic_desigh</title>

<link rel="stylesheet" type="text/css" href="css/default.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="css/home.css" type="text/css" charset="utf-8" />
<script src="js/jquery-1.8.3.js"></script>
<!-- Start VisualLightBox.com HEAD section -->
		<link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
		<link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
		<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
<!-- End VisualLightBox.com HEAD section -->
<script src="js/modernizr.custom.js"></script>

<script>
	var page="Protsolios";
</script>

</head>
<style>
	.content{
		width:78%;
		margin-left:21%;
		background-color:#F6F6F6;
		position:absolute;
		
		top:100%;
	
			
		}
		.con1{
			padding-top:5%;
			width:70%;
			margin-left:15%;
			font-family:Arial, Helvetica, sans-serif;
			font-size:25px;
			color:#FFFFFF;	
			padding-bottom:5%;
			background-color:#3A3A3A;
			text-align:center;
			}
		
		
		.topcon{
			height:70px;
			background-color:#3A3A3A;	
			}
		
		.pop{
			width:21.3%;
			float:left;
			margin-left:3%;
			margin-bottom:3%;
				
			}

</style>

<body>

<!---------------------------------------Slide images----------------------------------------------->

<div class="main">
  <ul id="cbp-bislideshow" class="cbp-bislideshow">
    <li><img src="images/background/001.jpg" /></li>
     <li><img src="images/background/002.jpg" /></li>    
    
  </ul>
  <div id="cbp-bicontrols" class="cbp-bicontrols"> <span class="cbp-biprev"></span> <span class="cbp-bipause"></span> <span class="cbp-binext"></span> </div>
</div>
<script src="js/jquery.imagesloaded.min.js"></script> 
<script src="js/cbpBGSlideshow.min.js"></script> 
<script>

			$(function() {

				cbpBGSlideshow.init();

			});

		</script> 

<!------------------------------------Slide images------------------------------------------------------>



<div class="content"> 
	
	<div class="con1">
    	WORKSHOP / POSITIONING / CREATIVE STRATEGY / NAMING / ENGAGEMENT STRATEGY / CONSULTING 
    </div>
    <div style="overflow:auto; padding-top:10%; padding-bottom:10%;">
	<!-- Start VisualLightBox.com BODY section id=1 -->
    	<div class="pop">
        	<a class="vlightbox1" href="index_files/big/1.jpg"><img src="index_files/small/1.jpg" alt="1" width="100%"/></a>
        </div>
        <div class="pop">
    		<a class="vlightbox1" href="index_files/big/2.jpg"><img src="index_files/small/2.jpg" alt="2" width="100%"/></a>
        </div>
    	<div class="pop">
        
        	<a class="vlightbox1" href="index_files/big/3.jpg"><img src="index_files/small/3.jpg" alt="3" width="100%"/></a>
        </div>
    	<div class="pop">
        	<a class="vlightbox1" href="index_files/big/4.jpg"><img src="index_files/small/4.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/5.jpg"><img src="index_files/small/5.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/6.jpg"><img src="index_files/small/6.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/7.jpg"><img src="index_files/small/7.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/8.jpg"><img src="index_files/small/8.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/9.jpg"><img src="index_files/small/9.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/10.jpg"><img src="index_files/small/10.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/11.jpg"><img src="index_files/small/11.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/12.jpg"><img src="index_files/small/12.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/13.jpg"><img src="index_files/small/13.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/14.jpg"><img src="index_files/small/14.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/15.jpg"><img src="index_files/small/15.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/16.jpg"><img src="index_files/small/16.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/17.jpg"><img src="index_files/small/17.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/18.jpg"><img src="index_files/small/18.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/19.jpg"><img src="index_files/small/19.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/20.jpg"><img src="index_files/small/20.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/21.jpg"><img src="index_files/small/21.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/22.jpg"><img src="index_files/small/22.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/23.jpg"><img src="index_files/small/23.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/24.jpg"><img src="index_files/small/24.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/25.jpg"><img src="index_files/small/25.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/26.jpg"><img src="index_files/small/26.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/27.jpg"><img src="index_files/small/27.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/28.jpg"><img src="index_files/small/28.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/29.jpg"><img src="index_files/small/29.jpg" alt="4" width="100%"/></a>
        </div>
        <div class="pop">
        	<a class="vlightbox1" href="index_files/big/30.jpg"><img src="index_files/small/30.jpg" alt="4" width="100%"/></a>
        </div>
    
	<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
	<!-- End VisualLightBox.com BODY section -->
	</div>	
<div>
    
	<?php
		include('layout/footer.php');
	 ?>
</div>
</div>
   
   
   
		<div>
	<?php 
		include('layout/header.php');
	
	?>

</div>
<div>
	<?php 
		include('layout/menutop.php');
	
	?>

</div>


</body>
</html>
