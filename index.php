
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery-1.8.3.js"></script>

<script src="js/jquery.sticky.js"></script>

<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="font/kozuka/fonts.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="font/TrajanPro-Regular/fonts.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="font/windsong/fonts.css" type="text/css" charset="utf-8" />
<!-- Start VisualLightBox.com HEAD section -->
<link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
<link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
<!-- End VisualLightBox.com HEAD section -->

<script src="js/jquery.corner.js"></script>
<script src="js/gray_scale_imge.js"></script>
<!--Gray Scale Image-->
<script type="text/javascript">
 $(window).load(function() {
	$('.gray img').each(function() {
  $(this).wrap('<div>').clone().addClass('gotcolors').css({'position': 'absolute', 'opacity' : 1 }).insertBefore(this);
  this.src = grayscale(this.src);
 }).animate({opacity: 1}, 500);
	
});
	
$(document).ready(function() {
	$(".gray").hover(
		function() {
			$(this).find('.gotcolors').stop().animate({opacity: 1}, 200);
		}, 
		function() {
			$(this).find('.gotcolors').stop().animate({opacity: 0}, 500);
		}
	);
	
});

</script>
	 
<!--Gray Scale Image-->        


<style>
body, html {
	margin:0;
	padding:0;
}
	
		.content{
			width:70%;
			margin-left:26%;
			overflow:auto;
			}
		.holdbox{
			overflow:auto;	
			}
			.box1{
				border: 7px solid #CCC;
				width:26.1%;
				float:left;
				margin-left:15%;
				text-align:center;
				
				}
			.gray{
				position:relative;	
				}
			.box2{
				border: 7px solid #CCC;
				width:26.1%;
				float:right;
				margin-right:15%;
				text-align:center;
				
				}
			.textbox1{
				font-family:Conv_kozgoprobold;
				margin:10px auto;
				color:#FFF;
				font-size:20px;
				padding-top:1%;
				padding-bottom:1%;	
				}
			.textbox2{
				font-size:22px;
				padding-top:1%;
				padding-bottom:1%;
				color:#FFF;
				}
				
		.unbox1{
			font-family:'Conv_TrajanPro-Regular',Sans-Serif;
			width:29%;
			margin-left:13%;
			text-align:center;
			float:left;
			font-size:11px;
			margin-top:7%;
			color:#C3CCCC;
			}
		.unbox1 a{
			color:#000;
			text-decoration:none;
			
			}
		.unbox1 a:hover{
			color:#C3CCCC;
			}
		.untext1{
			text-align:justify;
			}
		.unbox2{
			font-family:'Conv_TrajanPro-Regular',Sans-Serif;
			width:28%;
			float:right;
			text-align:center;
			margin-right:12%;
			font-size:11px;
			margin-top:7%;	
			color:#C3CCCC;
			}
		.unbox2 a{
			color:#000;
			text-decoration:none;
				
			}
		.unbox2 a:hover{
			color:#C3CCCC;
			}
		.untext2{
			text-align:justify;	
			}
		.holdbuttom{
			background-color:#363636;
			padding-top:3%;
			padding-bottom:3%;
			margin-top:5%;
			}
			.textbuttom{
				font-family:'Conv_TrajanPro-Regular',Sans-Serif;
				border-top:1px dotted #CCCCCC;
				border-bottom:1px dotted #CCCCCC;
				width:70%;
				margin-left:15%;
				color:#999;
				font-size:11px;	
				padding:1%;
				
				}
			.textbuttom1{
				font-family:'Conv_TrajanPro-Regular',Sans-Serif;
				border-bottom:1px dotted #CCCCCC;
				width:70%;
				margin-left:15%;
				color:#999;
				padding:1%;
				font-size:11px;	
				}
			.textbuttom2{
				font-family:'Conv_TrajanPro-Regular',Sans-Serif;
				border-bottom:1px dotted #CCCCCC;
				width:70%;
				margin-left:15%;
				
				padding:1%;
					
					
				}
</style>


<body style="background-color:#5B5C60">

    	<!-----------------------------------------Slide images------------------------------------------------->
     	 <div class="main">
            <ul id="cbp-bislideshow" class="cbp-bislideshow">
                 <li><img src="images/bagroundslide/1.png" /></li>
                 <li><img src="images/bagroundslide/2.png" /></li>
                 <li><img src="images/bagroundslide/3.png" /></li>
                 <li><img src="images/bagroundslide/4.png" /></li>
                 
                                
            </ul>
        </div>
		<!-- imagesLoaded jQuery plugin by @desandro : https://github.com/desandro/imagesloaded -->
		<script src="js/jquery.imagesloaded.min.js"></script>
		<script src="js/cbpBGSlideshow.min.js"></script>
		<script>
			$(function() {
				cbpBGSlideshow.init();
			});
		</script>
        

<!------------------------------------Slide images------------------------------------------------------>

        <div class="content">
        	<div class="holdbox">
                <!-- Start VisualLightBox.com BODY section id=1 -->
                    
                        <div class="box1">
                    		<div class="gray">
                            <a  class="vlightbox1" href="images/2.jpg">
                            	                           	
                                <img src="images/2.jpg" width="100%"/>
                            	
                            </a> 
                            </div>
                            <div class="textbox1">
                        		Printing &  Advertising
                        	</div> 
                                
                        </div>
                        
                        
                        
                        <div class="box2">
                        	<div class="gray">
                            <a class="vlightbox1"  href="images/1.jpg">
                            <img src="images/1.jpg" width="100%"/></a>
                            </div>
                             <div class="textbox2">
                        		graphic <span class="windsong">design</span>
                        	</div> 
                        	
                      
                        </div>
                    
                <!-- End VisualLightBox.com BODY section -->
                
         
        	</div>
            
            <div style="overflow:auto;">
            
                    <div class="unbox1">
                         | <a href="http://printing.gloryprintinghouse.com">GLORY PRINTING HOUSE</a> | <<<
                                    <br />
                                    <br />
                                   <span class="untext1"> Our talented in-house graphic design team will work with you to create the most effectively designed, printed piece.</span>
                    </div>
                    
                    
                    <div class="unbox2">
                    	 | <a href="http://design.gloryprintinghouse.com">GLORY AGENCY</a> |  <<<
                            <br />
                            <br />
                           <span class="untext2"> Our broad range of state-of-the-art equipment allows us to produce the highest quality product at the most cost efficient price.</span>
                    </div>
            </div>
        	
        </div>
        <div class="holdbuttom">
                <div class="textbuttom">
                    All material published on this website is copyright of Glory Agency and may not be copied, republished or reproduced in 					any form without permission, except for personal, non-commercial use.
                
                </div>
                <div class="textbuttom1">
                	#06, Group 1, Sangkat Steung Meanchey, Khan Meanchey, Phnom Penh, Kingdom of Cambodia.&nbsp;&nbsp;&nbsp;
                    Phone: +855 17 24 51 88 / 12 82 68 61
					<br />
                    Email: gloryprintinghouse@gmail.com
                    Website: www.gloryprintinghouse.com &nbsp;&nbsp;&nbsp;
                </div>
               	<div class="textbuttom2">
                	<table>
                    	<tr>
                    		<td valign="top" style="color:#999; font-size:11px;">Follow Us : </td>
                            <td valign="top"><a href="#"><img src="images/follow_us/ffb.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/ftw.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/google+.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/linkedIn.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/pinterest.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/rss.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/StumbleUpon.png" /></a></td>
                            <td valign="top"><a href="#"><img src="images/follow_us/tumble.png" /></a></td>
                    	</tr>
                    </table>
                
                </div>
        </div>

</body>